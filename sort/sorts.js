const Config = require('../config');

registerSort = () => {
    Config.addSort('lph', lphSort);
    Config.addSort('lpd', lphSort);
    Config.addSort('ppd', lphSort);
    Config.addSort('priceH', priceHSort);
    Config.addSort('priceL', priceLSort);
    Config.addSort('le', leSort);
    Config.addSort('hourH', hourHSort);
    Config.addSort('hourL', hourLSort);
    Config.addSort('lpp', lppSort);
    Config.addSort('lpppd', lpppdSort);
    Config.addSort('newest', newest);
}

const newest = (plant1, plant2) => {
    return plant2.updatedAt - plant1.updatedAt;
}

const lppSort = (plant1, plant2) => {
    const plant1LpP = plant1.startingPrice / plant1.config.farm.le;
    const plant2LpP = plant2.startingPrice / plant2.config.farm.le;
    return plant1LpP - plant2LpP;
}

const lpppdSort = (plant1, plant2) => {
    const plant1LpPpD = plant1.startingPrice / ((plant1.config.farm.le / plant1.config.farm.hours) * 24);
    const plant2LpPpD = plant2.startingPrice / ((plant2.config.farm.le / plant2.config.farm.hours) * 24);
    return plant1LpPpD - plant2LpPpD;
}

const hourHSort = (plant1, plant2) => {
    return plant2.config.farm.hours - plant1.config.farm.hours;
}

const hourLSort = (plant1, plant2) => {
    return plant1.config.farm.hours - plant2.config.farm.hours;
}

const leSort = (plant1, plant2) => {
    return plant2.config.farm.le - plant1.config.farm.le;
}

const priceHSort = (plant1, plant2) => {
    return plant2.startingPrice - plant1.startingPrice;
}

const priceLSort = (plant1, plant2) => {
    return plant1.startingPrice - plant2.startingPrice;
}

const lphSort = (plant1, plant2) => {
    const plant2LPH = plant2.config.farm.le / plant2.config.farm.hours;
    const plant1LPH = plant1.config.farm.le / plant1.config.farm.hours;
    return plant2LPH - plant1LPH;
}

module.exports = registerSort;
