require('dotenv').config();

class Config {
    static _registeredSorts = [];

    static loadConfig = () => {
        this.type = (() => {
            if (process.env.Type && process.env.Type === '2') {
                return '2'
            }
            return '1';
        })();
        this.lePerPvu = parseFloat(process.env.LEperPVU);
        this.minPrice = parseFloat(process.env.MinPrice);
        this.maxPrice = parseFloat(process.env.MaxPrice);
        this.minHours = parseFloat(process.env.MinHours);
        this.maxHours = parseFloat(process.env.MaxHours);
        this.minLph = parseFloat(process.env.MinLpH);
        this.maxLph = parseFloat(process.env.MaxLpH);
        this.minLpp = parseFloat(process.env.MinLpP);
        this.maxLpp = parseFloat(process.env.MaxLpP);
        this.maxOutput = parseInt(process.env.MaxOutput);
        this.validate = process.env.Validate && process.env.Validate.toLowerCase() === true.toString();
        this.fastOutput = process.env.FastOutput && process.env.FastOutput.toLowerCase() === true.toString();
        this.token = process.env.Token;
        this.marketApiUrl = 'https://backend-farm.plantvsundead.com/get-plants-filter-v2';
        this.itemApiUrl = 'https://backend-farm.plantvsundead.com/get-plant-detail-v2';
        this.marketBaseUrl = this.type === '2'
            ? 'https://marketplace.plantvsundead.com/farm#/mother-tree/'
            : 'https://marketplace.plantvsundead.com/farm#/plant/';
        this.elements = (() => {
            let elements = process.env.Elements;
            if (!elements || elements === '') {
                return null;
            }
            return elements;
        })();
        this.rarities = (() => {
            let rarities = process.env.Rarities;
            if (!rarities || rarities === '') {
                return null;
            }
            return rarities;
        })();
        this.sort = (!process.env.Sort || process.env.Sort === '')
            ? 'lph'
            : process.env.Sort.toLowerCase();
        this.stats = (() => {
            const stats = process.env.Stat.split(',');
            const res = [];

            for (const stat of stats) {
                if (stat !== '') {
                    res.push(stat);
                }
            }

            return res;
        })();
        this.maxInsertHour = (() => {
            if (!process.env.MaxInsertHour || process.env.MaxInsertHour === '') {
                return 0;
            }
            return parseInt(process.env.MaxInsertHour);
        })();
        this.plantId = (() => {
            const stats = process.env.PlantId.split(',');
            const res = [];

            for (const stat of stats) {
                if (stat !== '') {
                    res.push(stat);
                }
            }

            return res;
        })();
    }

    static addSort(name, func) {
        this._registeredSorts[name.toLowerCase()] = func;
    }

    static getSort() {
        if (this._registeredSorts[this.sort]) {
            return this._registeredSorts[this.sort];
        } else {
            throw `Invalid sort: ${this.sort}`;
        }
    }
}

module.exports = Config;
