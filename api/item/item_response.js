class ItemResponse {
    constructor(res) {
        if (res.data.status !== 0) {
            throw 'Invalid market data';
        }
        this.item = res.data.data;
    }

    isValid = () => {
        return !!this.item.startingPrice;

    }
}

module.exports = ItemResponse;