const Config = require('../../config');
const axios = require('axios');

class ItemRequest {
    defaultHeaders = {
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:92.0) Gecko/20100101 Firefox/92.0',
        'Accept': 'application/json, text/plain, */*',
        'Accept-Language': 'en-GB,en;q=0.7,en-US;q=0.3',
        'Authorization': 'Bearer Token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwdWJsaWNBZGRyZXNzIjoiMHg1ODkwOGM4MWI2NDViNWYyYzU0ZWM0ZjhjMGYxNTAyMTM4YmI1MGE4IiwibG9naW5UaW1lIjoxNjMzNTQ0MTIwMDEzLCJjcmVhdGVEYXRlIjoiMjAyMS0wOS0yNyAyMTo0MjoxMiIsImlhdCI6MTYzMzU0NDEyMH0.SM5IHHLLUHBNIoYGZHwioK7HCHYRzHecBKiCfwYQhFM',
        'Origin': 'https://marketplace.plantvsundead.com',
        'Connection': 'keep-alive',
        'Referer': 'https://marketplace.plantvsundead.com/',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-site',
        'Cache-Control': 'max-age=0',
        'TE': 'trailers'
    };
    queryParams = {
        type: Config.type,
    }

    constructor(id) {
        this.queryParams['plantId'] = id;
    }

    call = async () => {
        return await axios.get(
            Config.itemApiUrl,
            {
                headers: this.defaultHeaders,
                params: this.queryParams,
            }
        );
    };
}

module.exports = ItemRequest;