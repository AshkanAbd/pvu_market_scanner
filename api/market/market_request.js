const Config = require('../../config');
const axios = require('axios');

class MarketRequest {
    defaultHeaders = {
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:92.0) Gecko/20100101 Firefox/92.0',
        'Accept': 'application/json, text/plain, */*',
        'Accept-Language': 'en-GB,en;q=0.7,en-US;q=0.3',
        'Authorization': Config.token,
        'Origin': 'https://marketplace.plantvsundead.com',
        'Connection': 'keep-alive',
        'Referer': 'https://marketplace.plantvsundead.com/',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-site',
        'TE': 'trailers',
    };
    queryParams = {
        offset: 0,
        limit: 100000,
        type: Config.type,
    }

    call = async () => {
        if (Config.elements) {
            this.queryParams['elements'] = Config.elements;
        }
        if (Config.rarities) {
            this.queryParams['rarities'] = Config.rarities;
        }
        return await axios.get(
            Config.marketApiUrl,
            {
                headers: this.defaultHeaders,
                params: this.queryParams,
            }
        );
    }
}

module.exports = MarketRequest;
