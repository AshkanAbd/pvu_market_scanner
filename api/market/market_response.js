const Config = require('../../config');
const ItemRequest = require('../item/item_request');
const ItemResponse = require('../item/item_response');
const {log, logc, changeColor, colors} = require('../../logger');
const Plant = require('pvu_plant');

class MarketResponse {
    constructor(res) {
        if (res.data.status !== 0) {
            throw 'Invalid market data';
        }
        this.plants = res.data.data;
        this.result = [];
    }

    sort = () => {
        this.plants.sort(Config.getSort());
    }

    calculate = async () => {
        let counter = 0;

        for (const plant of this.plants) {
            const lph = (plant.config.farm.le / plant.config.farm.hours);
            plant.lph = lph;
            plant.plantId = Plant.getPlantByNumber(plant.id).getId();

            if (Config.plantId.length !== 0) {
                if (!Config.plantId.includes(plant.plantId)) {
                    continue;
                }
            }

            let date = new Date();
            date -= (Config.maxInsertHour * 60 * 60 * 1000);
            if (date >= (plant.updatedAt * 1000)) {
                continue;
            }

            if (!this._inBound(Config.minPrice, Config.maxPrice, plant.startingPrice)) {
                continue;
            }
            if (!this._inBound(Config.minHours, Config.maxHours, plant.config.farm.hours)) {
                continue;
            }
            if (!this._inBound(Config.minLph, Config.maxLph, lph)) {
                continue;
            }
            if (!this._inBound(Config.minLpp, Config.maxLpp, (plant.startingPrice / plant.config.farm.le))) {
                continue;
            }

            let shouldAdd = false;

            statChecker:{
                for (const stat of Config.stats) {
                    if (plant.config.stats[stat] === 0) {
                        break statChecker;
                    }
                }
                shouldAdd = true;
            }

            if (!shouldAdd) {
                continue;
            }

            if (Config.validate && !await this._isValidItem(plant)) {
                continue;
            }

            if (Config.fastOutput) {
                this.print(plant);
            }
            this.result.push(plant);
            if (counter >= Config.maxOutput) {
                break;
            }
            counter++;
        }
        if (Config.fastOutput && this.result.length === 0) {
            logc(colors.fg.red, 'No plant found for you.');
        }
    }

    print = (plant) => {
        log(
            changeColor(colors.fg.magenta, Config.marketBaseUrl + plant.id),
            'Created at ', changeColor(colors.fg.cyan, new Date(plant.updatedAt * 1000).toLocaleString())
        );
        log(
            plant.plantId, 'id,', plant.config.stats.type, 'type,', this._round(plant.lph), 'LpH,',
            this._round(plant.lph * 24), 'LpD,', this._round(plant.lph * 24 / Config.lePerPvu), 'PpD,',
            this._round(plant.startingPrice), 'Price,', plant.config.farm.le, 'LE,', plant.config.farm.hours, 'H,',
            this._round(plant.startingPrice / plant.config.farm.le, 5), 'LpP',
            this._round(plant.startingPrice / (plant.lph * 24), 5), 'LpPpD',
        );
        log(
            plant.config.stats.damagePhysics, 'damagePhysics,', plant.config.stats.damageMagic, 'damageMagic,',
            plant.config.stats.damagePure, 'damagePure,', plant.config.stats.defPhysics, 'defPhysics,',
            plant.config.stats.defMagic, 'defMagic,', plant.config.stats.hp, 'hp,',
            plant.config.stats.damageHpLoss, 'damageHpLoss,', plant.config.stats.damageHpRemove, 'damageHpRemove,'
        );
    }

    printAll = () => {
        if (this.result.length === 0) {
            logc(colors.fg.red, 'No plant found for you.');
            return;
        }
        this.result.forEach(this.print);
    }

    _compare = (v1, v2, c) => {
        if (c) {
            return v1 === 0 || (v1 !== 0 && v1 >= v2);
        } else {
            return v1 === 0 || (v1 !== 0 && v1 <= v2);
        }
    }

    _inBound = (min, max, v) => {
        return this._compare(min, v, false) && this._compare(max, v, true);
    }

    _round = (num, float = 2) => {
        const floatCount = Math.pow(10, float);
        return Math.round(num * floatCount) / floatCount;
    }

    _isValidItem = async (plant) => {
        const itemRequest = new ItemRequest(plant.id);
        const itemResponse = new ItemResponse(await itemRequest.call());
        return itemResponse.isValid();
    }
}

module.exports = MarketResponse;
