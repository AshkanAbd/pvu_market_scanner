const Config = require('./config');
const MarketRequest = require('./api/market/market_request');
const MarketResponse = require('./api/market/market_response');
const {logc, changeColor, colors} = require('./logger');
require('./sort/sorts')();

const main = async () => {
    Config.loadConfig();
    logc(colors.fg.green, 'Registered sorts:', changeColor(colors.fg.magenta, Object.keys(Config._registeredSorts).join(', ')));
    logc(colors.fg.green, 'Selected sort:', changeColor(colors.fg.crimson, Config.sort));
    if (Config.validate) {
        logc(colors.fg.red, 'Validate items is enable, this may reduce performance!');
    }
    const marketRequest = new MarketRequest();
    logc(colors.fg.cyan, 'Fetching items...');
    const marketResponse = new MarketResponse(await marketRequest.call());
    logc(colors.fg.cyan, 'Sorting items...');
    marketResponse.sort();
    logc(colors.fg.cyan, 'Calculating...');
    await marketResponse.calculate();
    if (!Config.fastOutput) {
        logc(colors.fg.cyan, 'Output:');
        marketResponse.printAll();
    }
}

main()
    .then()
    .catch(e => {
        logc(colors.fg.red, 'Error: ');
        console.log(e);
    });
