const PlantInfo = {
    '00': {
        id: '0',
        element: 'Fire',
        baseLE: [400, 440, 511, 701],
        hour: '48',
        step: '1',
        type: 'Plant'
    },
    '01': {
        id: '1',
        element: 'Fire',
        baseLE: [400, 440, 511, 701],
        hour: '48',
        step: '1',
        type: 'Plant'
    },
    '02': {
        id: '2',
        element: 'Ice',
        baseLE: [500, 550, 591, 751],
        hour: '60',
        step: '1',
        type: 'Plant'
    },
    '03': {
        id: '3',
        element: 'Electro',
        baseLE: [500, 550, 591, 751],
        hour: '48',
        step: '1',
        type: 'Plant'
    },
    '04': {
        id: '4',
        element: 'Water',
        baseLE: [950, 1040, 1111, 1301],
        hour: '108',
        step: '1',
        type: 'Plant'
    },
    '05': {
        id: '5',
        element: 'Water',
        baseLE: [950, 1040, 1111, 1301],
        hour: '108',
        step: '1',
        type: 'Plant'
    },
    '06': {
        id: '6',
        element: 'Ice',
        baseLE: [500, 550, 591, 751],
        hour: '60',
        step: '1',
        type: 'Plant'
    },
    '07': {
        id: '7',
        element: 'Fire',
        baseLE: [400, 440, 511, 701],
        hour: '48',
        step: '1',
        type: 'Plant'
    },
    '08': {
        id: '8',
        element: 'Electro',
        baseLE: [500, 550, 591, 751],
        hour: '48',
        step: '1',
        type: 'Plant'
    },
    '09': {
        id: '9',
        element: 'Wind',
        baseLE: [750, 800, 861, 1051],
        hour: '72',
        step: '1',
        type: 'Plant'
    },
    '10': {
        id: '10',
        element: 'Wind',
        baseLE: [750, 800, 861, 1051],
        hour: '72',
        step: '1',
        type: 'Plant'
    },
    '11': {
        id: '11',
        element: 'Parasite',
        baseLE: [900, 950, 1011, 1151],
        hour: '120',
        step: '1',
        type: 'Plant'
    },
    '12': {
        id: '12',
        element: 'Parasite',
        baseLE: [900, 950, 1011, 1151],
        hour: '120',
        step: '1',
        type: 'Plant'
    },
    '13': {
        id: '13',
        element: 'Parasite',
        baseLE: [900, 950, 1011, 1151],
        hour: '120',
        step: '1',
        type: 'Plant'
    },
    '14': {
        id: '14',
        element: 'Dark',
        baseLE: [1200, 1840, 2211, 2401],
        hour: '192',
        step: '10',
        type: 'Plant'
    },
    '15': {
        id: '15',
        element: 'Electro',
        baseLE: [500, 550, 591, 751],
        hour: '48',
        step: '1',
        type: 'Plant'
    },
    '16': {
        id: '16',
        element: 'Wind',
        baseLE: [900, 950, 1011, 1151],
        hour: '96',
        step: '1',
        type: 'Plant'
    },
    '17': {
        id: '17',
        element: 'Fire',
        baseLE: [650, 700, 811, 1001],
        hour: '72',
        step: '1',
        type: 'Plant'
    },
    '18': {
        id: '18',
        element: 'Light',
        baseLE: [1200, 1250, 1311, 1401],
        hour: '240',
        step: '1',
        type: 'Plant'
    },
    '19': {
        id: '19',
        element: 'Light',
        baseLE: [1200, 1250, 1311, 1401],
        hour: '240',
        step: '1',
        type: 'Plant'
    },
    '20': {
        id: '20',
        element: 'Light',
        baseLE: [1600, 1650, 1711, 1901],
        hour: '312',
        step: '1',
        type: 'Plant'
    },
    '21': {
        id: '21',
        element: 'Light',
        baseLE: [1600, 1650, 1711, 1901],
        hour: '312',
        step: '1',
        type: 'Plant'
    },
    '22': {
        id: '22',
        element: 'Parasite',
        baseLE: [1300, 1350, 1411, 1551],
        hour: '168',
        step: '1',
        type: 'Plant'
    },
    '23': {
        id: '23',
        element: 'Parasite',
        baseLE: [1300, 1350, 1411, 1551],
        hour: '168',
        step: '1',
        type: 'Plant'
    },
    '24': {
        id: '24',
        element: 'Parasite',
        baseLE: [1300, 1350, 1411, 1551],
        hour: '168',
        step: '1',
        type: 'Plant'
    },
    '25': {
        id: '25',
        element: 'Metal',
        baseLE: [3500, 4240, 4711, 5101],
        hour: '336',
        step: '10',
        type: 'Plant'
    },
    '26': {
        id: '26',
        element: 'Metal',
        baseLE: [3500, 4240, 4711, 5101],
        hour: '336',
        step: '10',
        type: 'Plant'
    },
    '27': {
        id: '27',
        element: 'Metal',
        baseLE: [5500, 6340, 6711, 7001],
        hour: '480',
        step: '10',
        type: 'Plant'
    },
    '28': {
        id: '28',
        element: 'Metal',
        baseLE: [5500, 6340, 6711, 7001],
        hour: '480',
        step: '10',
        type: 'Plant'
    },
    '29': {
        id: '29',
        element: 'Ice',
        baseLE: [800, 850, 911, 1151],
        hour: '96',
        step: '1',
        type: 'Plant'
    },
    '30': {
        id: '30',
        element: 'Fire',
        baseLE: [650, 700, 811, 1001],
        hour: '72',
        step: '1',
        type: 'Plant'
    },
    '31': {
        id: '31',
        element: 'Dark',
        baseLE: [1200, 1840, 2211, 2401],
        hour: '192',
        step: '10',
        type: 'Plant'
    },
    '32': {
        id: '32',
        element: 'Electro',
        baseLE: [650, 700, 811, 1001],
        hour: '60',
        step: '1',
        type: 'Plant'
    },
    '33': {
        id: '33',
        element: 'Dark',
        baseLE: [1400, 2040, 2411, 2701],
        hour: '216',
        step: '10',
        type: 'Plant'
    },
    '34': {
        id: '34',
        element: 'Electro',
        baseLE: [650, 700, 811, 1001],
        hour: '60',
        step: '1',
        type: 'Plant'
    },
    '35': {
        id: '35',
        element: 'Dark',
        baseLE: [1400, 2040, 2411, 2701],
        hour: '216',
        step: '10',
        type: 'Plant'
    },
    '36': {
        id: '36',
        element: 'Water',
        baseLE: [950, 1040, 1111, 1301],
        hour: '108',
        step: '1',
        type: 'Plant'
    },
    '37': {
        id: '37',
        element: 'Wind',
        baseLE: [900, 950, 1011, 1151],
        hour: '96',
        step: '1',
        type: 'Plant'
    },
    '38': {
        id: '38',
        element: 'Water',
        baseLE: [1050, 1140, 1211, 1401],
        hour: '120',
        step: '1',
        type: 'Plant'
    },
    '39': {
        id: '39',
        element: 'Water',
        baseLE: [1050, 1140, 1211, 1401],
        hour: '120',
        step: '1',
        type: 'Plant'
    },
    '90': {
        id: '90',
        element: 'Fire',
        baseLE: [750, 1040, 1211, 1401],
        hour: '48',
        step: '5',
        type: 'Mother tree'
    },
    '91': {
        id: '91',
        element: 'Light',
        baseLE: [1400, 1690, 1851, 2021],
        hour: '240',
        step: '5',
        type: 'Mother tree'
    },
    '92': {
        id: '92',
        element: 'Ice',
        baseLE: [1050, 1340, 1511, 1701],
        hour: '96',
        step: '5',
        type: 'Mother tree'
    },
    '93': {
        id: '93',
        element: 'Dark',
        baseLE: [2600, 2890, 3061, 3201],
        hour: '216',
        step: '5',
        type: 'Mother tree'
    }
};
module.exports = PlantInfo;
