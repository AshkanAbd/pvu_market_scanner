class Rarity {
    constructor(rarityNumber) {
        if (typeof rarityNumber === 'number') {
            this.rarityNumber = rarityNumber;
        } else if (typeof rarityNumber === 'string') {
            this.rarityNumber = parseInt(rarityNumber);
        } else throw 'Plant number should be number or string';
    }

    getRarity() {
        const rarityNumber = this.getRarityCode();
        switch (rarityNumber) {
            case 0:
                return 'Common';
            case 1:
                return 'Uncommon';
            case 2:
                return 'Rare';
            default:
                return 'Mythic';
        }
    }

    getRarityCode() {
        const rarityCode = this.rarityNumber;
        if (rarityCode >= 0 && rarityCode <= 59) {
            return 0;
        } else if (rarityCode >= 60 && rarityCode <= 88) {
            return 1;
        } else if (rarityCode >= 89 && rarityCode <= 98) {
            return 2;
        } else {
            return 3;
        }
    }

    getLowestNumber() {
        const rarityCode = this.getRarityCode();
        switch (rarityCode) {
            case 1:
                return 0;
            case 2:
                return 60;
            case 3:
                return 89;
            default:
                return 99;
        }
    }

    getRarityColor() {
        const rarityCode = this.getRarityCode();
        switch (rarityCode) {
            case 1:
                return '#198754';
            case 2:
                return '#0d6efd';
            case 3:
                return '#dc3545';
            default:
                return '#6610f2';
        }
    }

    getRarityObject() {
        return {
            'rarityType': this.getRarity(),
            'rarityNumber': this.getRarityCode(),
            'rarityColor': this.getRarityColor(),
        }
    }
}

module.exports = Rarity;
