const PlantInfo = require('./PlantInfo');
const Rarity = require('./Rarity');

class Plant {
    static getPlantByNumber(plantId) {
        const plant = new Plant();

        if (typeof plantId === 'number') {
            plant.plantId = plantId.toString();
        } else if (typeof plantId === 'string') {
            plant.plantId = plantId;
        } else throw 'Plant number should be number or string';

        return plant;
    }

    getId() {
        return this.plantId.substr(3, 2);
    }

    getImageCode() {
        return this.plantId[5];
    }

    getRarityNumber() {
        return this.plantId.substr(6, 2);
    }

    getPlantInfo() {
        return PlantInfo[this.getId()];
    }

    getRarity() {
        if (!this.rarity) {
            this.rarity = new Rarity(this.getRarityNumber());
        }

        return this.rarity;
    }

    getLe() {
        return this.getPlantInfo().baseLE[this.getRarity().getRarityCode()] +
            this.getRarity().getLowestNumber() +
            (parseInt(this.getRarityNumber()) - this.getRarity().getLowestNumber()) *
            this.getPlantInfo().step;
    }

    getPlantObject() {
        return {
            PlantId: this.plantId,
            Id: this.getId(),
            Element: this.getPlantInfo().element,
            Hour: this.getPlantInfo().hour,
            Type: this.getPlantInfo().type,
            ImageCode: this.getImageCode(),
            RarityInfo: this.getRarity().getRarityObject(),
            Rarity: this.getRarityNumber(),
            LE: this.getLe(),
        }
    }
}

module.exports = Plant;
